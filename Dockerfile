# Use the latest version of Ubuntu as the base image
FROM ubuntu:latest

# Install Nginx and other dependencies
RUN apt-get update && apt-get install -y nginx

# Copy index.html
COPY index.html /var/www/html/

# Expose port 80 so that it can be accessed from outside the container
EXPOSE 80

# Start the Nginx server
CMD ["nginx", "-g", "daemon off;"]
